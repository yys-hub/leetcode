import java.util.ArrayList;
import java.util.List;

public class SimilarityStr {
    public static boolean findSimilarityStr(String str1, String str2) {
        int n = str1.length();
        int m = str2.length();
        if (m > n) {
            return false;
        }
        //将str2拆分成子串
        List<String> subStrList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < m; i++) {
            while (i < m && str2.charAt(i) != ' ') {
                stringBuilder.append(str2.charAt(i));
                i++;
            }
            String str = stringBuilder.toString();
            subStrList.add(str);
            stringBuilder = new StringBuilder();
        }
        int count = 0;
        for (int i = 0; i < subStrList.size(); i++) {
            if (isSonStr(str1,subStrList.get(i))){
                count++;
            }
            if (count == subStrList.size()){
                return true;
            }
        }
        return false;
    }

    public static boolean isSonStr(String masterStr, String sonStr) {
        int m = masterStr.length();
        int n = sonStr.length();
        int master = 0, son = 0;
        while (master < m) {
            if (sonStr.charAt(son) == masterStr.charAt(master)) {
                son++;
                master++;
                while (son < n && master < m && sonStr.charAt(son) == masterStr.charAt(master)) {
                    if (son == sonStr.length() - 1) {
                        return true;
                    }
                    son++;
                    master++;
                }
            }
            master++;
            son = 0;
        }
        return false;
    }

    public static void main(String[] args) {
        String str1 = "My name is LiLei";
        String str2 = "My LiLei";
        boolean similarityStr = findSimilarityStr(str1,str2);
        System.out.println(similarityStr);
    }
}
