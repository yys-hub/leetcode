package thirtyOneToForty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 作者: yys
 * 创建日期: 2024/5/22 17:06
 */
public class Title39 {

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> reultList = new ArrayList<>();
        Arrays.sort(candidates);
        int index = 0;
        if(candidates[index] > target){
            return null;
        }
        while (index < candidates.length){
            while (index+1 < candidates.length && candidates[index] + candidates[index+1] != target){
                index++;
            }
            index++;
        }
        return null;
    }
}