package thirtyOneToForty;

import java.util.ArrayList;
import java.util.List;

public class ThirtyOne {
//    public static void nextPermutation(int[] nums) {
//        int n = nums.length;
//        int right = n -1,count = 0;
//        boolean flag = false;
//        int temp,top;
//        while (right > 0){
//            if (nums[right-1] < nums[right]){
//                temp = nums[right-1];
//                nums[right-1] = nums[right];
//                nums[right] = temp;
//                break;
//            } else if (right >= 2 && nums[right-1] > nums[right] && nums[right-1] > nums[right-2] && nums[right-2] > nums[right]) {
//                temp = nums[right-2];
//                nums[right-2] = nums[right-1];
//                nums[right-1] = temp;
//                top = nums[right-1];
//                nums[right-1] = nums[right];
//                nums[right] = top;
//                break;
//            } else if (right >= 2 && nums[right-1] > nums[right] && nums[right-1] > nums[right-2] && nums[right-2] < nums[right]) {
//                temp = nums[right-2];
//                nums[right-2] = nums[right];
//                nums[right] = temp;
//                top = nums[right-1];
//                nums[right-1] = nums[right];
//                nums[right] = top;
//            } else {
//                flag = true;
//                count++;
//            }
//            right--;
//        }
//        if (flag && count == n-1){
//            Arrays.sort(nums);
//        }
//    }

    public static void nextPermutation(int[] nums) {
        int i = nums.length - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) {
            i--;
        }
        if (i >= 0) {
            int j = nums.length - 1;
            while (j >= 0 && nums[i] >= nums[j]) {
                j--;
            }
            swap(nums, i, j);
        }
        reverse(nums, i + 1);
    }

    public static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public static void reverse(int[] nums, int start) {
        int left = start, right = nums.length - 1;
        while (left < right) {
            swap(nums, left, right);
            left++;
            right--;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1, 3, 2};
        List<Integer> result = new ArrayList<>();
        nextPermutation(nums);
        for (int s : nums) {
            result.add(s);
        }
        System.out.println(result);
    }
}
