package thirtyOneToForty;

public class ThirtyFive {
    public static int searchInsert(int[] nums, int target) {
        int n = nums.length;
        int left = 0, right = n - 1, mid = 0, result = 0;
        while (left <= right) {
            mid = left + (right - left) / 2;
            //处理还剩2个元素时的情况
            if (right - left <= 1) {
                if (nums[left] == target) {
                    return left;
                } else if (nums[right] == target) {
                    return right;
                } else if (nums[left] > target) {
                    return result;
                } else if (nums[right] < target) {
                    right++;
                    result = right;
                    break;
                } else {
                    left++;
                    result = left;
                    break;
                }
            } else {  //还没找到目标元素且剩余元素没有到2就一直二分
                if (nums[mid] > target) {
                    right = mid;
                } else if (nums[mid] < target) {
                    left = mid;
                } else {
                    return mid;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 6};
        int target = 0;
        int result = searchInsert(nums, target);
        System.out.println(result);
    }
}
