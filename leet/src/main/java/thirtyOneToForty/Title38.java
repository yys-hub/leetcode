package thirtyOneToForty;

/**
 * 作者: yys
 * 创建日期: 2024/5/21 17:00
 */
public class Title38 {

    public String countAndSay(int n) {
        int i = 1;
        String result = "1";
        if (n != 1) {
            n--;
            while (i <= n) {
                result = read(result);
                i++;
            }
        }
        return result;
    }

    public String read(String str){
        int index = 0 ;
        char[] sz = {'1','2','3','4','5','6','7','8','9'};
        StringBuilder sb = new StringBuilder();
        while (index < str.length()){
            int startIndex = index;
            int endIndex = index+1;
            while (true){
                if ( endIndex <str.length() && smale(str.charAt(startIndex),str.charAt(endIndex))){
                    endIndex++;
                }
                else {
                    for(char c : sz){
                        if (str.charAt(index) == c){
                            int count = endIndex-startIndex;
                            char s = c;
                            sb.append(count);
                            sb.append(s);
                        }
                    }
                    index = endIndex - 1;
                    break;
                }
            }
            index++;
        }
        return sb.toString();
    }

    public Boolean smale(char a,char b){
        if (a == b){
            return true;
        }
        return false;
    }
}