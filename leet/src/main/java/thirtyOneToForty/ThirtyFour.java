package thirtyOneToForty;

import java.util.Arrays;

public class ThirtyFour {

    /**暴力解法
     *
     * @param nums
     * @param target
     * @return
     */
//    public static int[] searchRange(int[] nums, int target) {
//        int n = nums.length;
//        int start = -1, end = -1, mov = 0;
//        while (mov < n) {
//            if (nums[mov] == target) {
//                start = mov;
//                do {
//                    end = mov;
//                    mov++;
//                } while (mov < n && nums[mov] == target);
//                break;
//            }
//            mov++;
//        }
//        return new int[]{start,end};
//    }

    /**
     * 二分法
     *
     * @param nums
     * @param target
     * @return
     */
//    public static int[] searchRange(int[] nums, int target) {
//        int leftIdx = binarySearch(nums, target, true);
//        int rightIdx = binarySearch(nums, target, false) - 1;
//        if (leftIdx <= rightIdx && rightIdx < nums.length && nums[leftIdx] == target && nums[rightIdx] == target) {
//            return new int[]{leftIdx, rightIdx};
//        }
//        return new int[]{-1, -1};
//    }
//
//    public static int binarySearch(int[] nums, int target, boolean lower) {
//        int left = 0, right = nums.length - 1, ans = nums.length;
//        while (left <= right) {
//            int mid = (left + right) / 2;
//            if (nums[mid] > target || (lower && nums[mid] >= target)) {
//                right = mid - 1;
//                ans = mid;
//            } else {
//                left = mid + 1;
//            }
//        }
//
//        return ans;
//    }
    public static int[] searchRange(int[] nums, int target) {
        int n = nums.length;
        int left = 0, right = n - 1, mid = 0;
        if (n == 0){
            return new int[]{-1,-1};
        }
        if (n == 1){
            if (nums[0] == target){
                return new int[]{0,0};
            }else {
                return new int[]{-1,-1};
            }
        }
        while (left <= right) {
            mid = (right + left)/2;
            if (mid == 0){
                mid++;
            }
            if (nums[mid] > target) {
                right = mid;
            } else if (nums[mid] < target) {
                left = mid;
            } else if (nums[mid] == target) {
                left = mid;
                right = mid;
                do {
                    right++;
                } while (right < n && nums[right] == target);
                do {
                    left--;
                } while (left > 0 && nums[left] == target);
                break;
            } else {
                left = -1;
                right = -1;
            }
        }
        return new int[]{left+1, right-1};
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        int target = 1;
        int[] result = searchRange(nums, target);
        System.out.println(Arrays.toString(result));
    }
}
