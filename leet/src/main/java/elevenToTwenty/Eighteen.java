package elevenToTwenty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Eighteen {

    /**
     * @param nums
     * @param target
     * @return
     */
    public static List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> dataList = new ArrayList<>();
        int len = nums.length;
        //数组排序
        Arrays.sort(nums);
        //遍历i
        for (int i = 0; i < len - 3; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            if (nums[i] > 0 && nums[i] > target) {
                break;
            }
            //遍历j
            for (int j = i + 1; j < len - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                if (nums[j] > 0 && nums[i] + nums[j] > target) {
                    break;
                }
                int left = j + 1, right = len - 1;
                while (left < right) {
                    int sum = nums[i] + nums[j] + nums[left] + nums[right];
                    if (sum > target || (right < len - 1 && nums[right] == nums[right + 1])) {
                        right--;
                    } else if (sum < target || (left > j + 1 && nums[left] == nums[left - 1])) {
                        left++;
                    } else {
                        List<Integer> data = new ArrayList<>();
                        data.add(nums[i]);
                        data.add(nums[j]);
                        data.add(nums[left]);
                        data.add(nums[right]);
                        dataList.add(data);
                        right--;
                        left++;
                    }
                }
            }
        }
        return dataList;
    }

    public static void main(String[] args) {
        int[] nums = {1000000000,1000000000,1000000000,1000000000};
        int target = -294967296;
        List<List<Integer>> dataList = fourSum(nums, target);
        System.out.printf(dataList.toString());
    }
}
