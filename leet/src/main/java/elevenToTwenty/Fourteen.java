package elevenToTwenty;

public class Fourteen {

    public static String longestCommonPrefix(String[] strs) {
        //找出最短的元素
        int m = 0,n = 0;
        boolean flag = false;
        StringBuffer sb = new StringBuffer();
        //1.先算出数据元素的平均长度
        for (String s : strs) {
            m = s.length() + m;
        }
        //2.找出最短元素的长度
        m = m/strs.length;
        for (String str : strs) {
            if (str.length() <= m) {
                m = str.length();
            }
        }
        while (n < m){
            for(int k=0;k< strs.length-1;k++){
                if (strs[0].charAt(n) != strs[k+1].charAt(n)){
                    flag = true;
                    break;
                }
            }
            sb.append(strs[0].charAt(n));
            if (flag){
                sb.deleteCharAt(n);
                break;
            }
            n++;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String[] strs = {"ab", "a"};
        System.out.printf(longestCommonPrefix(strs));
    }
}
