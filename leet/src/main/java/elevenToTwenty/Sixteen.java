package elevenToTwenty;

import java.util.Arrays;

public class Sixteen {

    /*
    暴力解法
     */
/*
    public static int threeSumClosest(int[] nums, int target) {
        int n = nums.length;
        int temp= 100000;
        int result = 0;
        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                for (int k = j + 1; k < n; k++) {
                    int m = Math.abs(nums[i] + nums[j] + nums[k] - target);
                    if (m < temp ){
                        temp = m;
                        result = nums[i] + nums[j] + nums[k];
                    }
                }
            }
        }
        return result;
    }
*/

    /*
    双指针
     */
    public static int threeSumClosest(int[] nums, int target) {
        int n = nums.length;
        //数组排序
        Arrays.sort(nums);
        //设置一个最大数
        int temp = 100000;
        //初始化返回结果值
        int result = 0;
        //枚举i
        for (int i = 0; i < n - 2; i++) {
            //如果与上一个i相同，直接跳过
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            //定义指针j,k
            int j = i + 1, k = n - 1;
            //j,k一定满足j<k
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                //更新与target相差最小的结果
                if (Math.abs(sum - target) < temp) {
                    temp = Math.abs(sum - target);
                    result = sum;
                }
                if (sum == target) {
                    return target;
                }
                //如果三数之和大于target 则k--，小于则j++
                else if (sum > target) {
                    k--;
                } else {
                    j++;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {-1, 2, 1, -4};
        int target = 1;
        int result = threeSumClosest(nums, target);
        System.out.println(result);
    }
}
