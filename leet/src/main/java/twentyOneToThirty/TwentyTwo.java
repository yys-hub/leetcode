package twentyOneToThirty;

import java.util.ArrayList;
import java.util.List;

public class TwentyTwo {

    /**
     * 1.逐级写括号 ()
     * @param n
     * @return
     */
    public static List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<String>();
        for (int i=1;i<=n;i++){
            genParentheses(i,0,new char[2*i],result);
        }
        return result;
    }
    public static void genParentheses(int i,int pos,char[] chars,List<String> result) {
        if (pos == i){
            result.add(new String(chars));
        }
        else  {
            chars[pos] = '(';
            genParentheses(i,pos + 1,chars,result);
            chars[pos] = ')';
            genParentheses(i,pos + 1,chars,result);
        }
    }

    public static void main(String[] args) {
        int n = 3;
        List<String> data = generateParenthesis(n);
        System.out.printf(data.toString());
    }
}
