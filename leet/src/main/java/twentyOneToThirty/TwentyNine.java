package twentyOneToThirty;

public class TwentyNine {

    public static int divide(int dividend, int divisor) {
        int count = 0;
        // 考虑被除数为最小值的情况
        if (dividend == Integer.MIN_VALUE) {
            if (divisor == 1) {
                return Integer.MIN_VALUE;
            }
            if (divisor == -1) {
                return Integer.MAX_VALUE;
            }
        }
        // 考虑除数为最小值的情况
        if (divisor == Integer.MIN_VALUE) {
            return dividend == Integer.MIN_VALUE ? 1 : 0;
        }
        // 考虑被除数为 0 的情况
        if (dividend == 0) {
            return 0;
        }
        if (dividend < 0 && divisor > 0) {
            dividend = -dividend;
            count = -countQuotient(divisor, dividend, count);
        } else if (dividend > 0 && divisor < 0) {
            divisor = -divisor;
            count = -countQuotient(divisor, dividend, count);
        } else if (dividend < 0 && divisor < 0) {
            dividend = -dividend;
            divisor = -divisor;
            count = countQuotient(divisor, dividend, count);
        } else {
            count = countQuotient(divisor, dividend, count);
        }
        return count;
    }

    public static int countQuotient(int divisor, int dividend, int count) {
        int temp = divisor;
        if (divisor > dividend){
            return 0;
        }
        if (dividend == divisor){
            return 1;
        }
        while (divisor <= dividend) {
            divisor = divisor + temp;
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int dividend = 5, divisor = 1;
        int result = divide(dividend, divisor);
        System.out.println(result);
    }
}
