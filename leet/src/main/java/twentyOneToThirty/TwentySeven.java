package twentyOneToThirty;

public class TwentySeven {

    public static int removeElement(int[] nums, int val) {
        int n = nums.length;
        if (n == 0) {
            return n;
        }
        int fast = 0, slow = 0;
        while (fast < n) {
            if (nums[fast] != val) {
                nums[slow] = nums[fast];
                slow++;
            }
            fast++;
        }
        return slow;
    }

    public static void main(String[] args) {
        int[] nums = {3,2,2,3};
        int val = 3;
        int result = removeElement(nums,val);
        System.out.println(result);
    }
}
